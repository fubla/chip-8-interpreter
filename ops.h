#ifndef OPS_H
#define OPS_H

void _0NNN(address); //SYS addr (ignore this)
void _00E0(void); //CLS - clear display
void _00EE(void); //RET -return
void _1NNN(address); //JP addr - jump
void _2NNN(address); //CALL addr - call subroutine
void _3XNN(byte, byte); //SE Vx, byte - skip next instruction if Vx == NN
void _4XNN(byte, byte); //SNE Vx, byte - skip next instruction if Vx != NN
void _5XY0(byte, byte); //SE Vx, Vy - skip next instruction if Vx == Vy
void _6XNN(byte, byte); //LD Vx, byte - load value to Vx
void _7XNN(byte, byte); //ADD Vx, byte - add value to Vx
void _8XY0(byte, byte); //LD Vx, Vy - load Vy to Vx
void _8XY1(byte, byte); //OR Vx, Vy - Vx = Vx OR Vy
void _8XY2(byte, byte); //AND Vx, Vy - Vx = Vx AND Vy
void _8XY3(byte, byte); //XOR Vx, Vy - Vx = Vx XOR Vy
void _8XY4(byte, byte); //ADD Vx, Vy - Vx = Vx + Vy
void _8XY5(byte, byte); //SUB Vx, Vy - Vx = Vx - Vy
void _8XY6(byte); //SHR Vx, 1
void _8XY7(byte, byte); //SUBN Vx, Vy - Vx = Vy - Vx
void _8XYE(byte); //SHL Vx, 1
void _9XY0(byte, byte); //SNE Vx, Vy - Skip next instruction if Vx != Vy
void _ANNN(address); //LD I, addr - set I to addr
void _BNNN(address); //JP V0, addr - jump to nnn + V0
void _CXNN(byte, byte); //RND Vx, byte - Set Vx = random byte AND kk
void _DXYN(byte, byte, byte); //DRW Vx, nibble - Draw n-byte sprite starting at address in I to coordinates Vx, Vy , VF = collision
void _EX9E(byte); //SKP Vx - Skip next instruction if key with value of Vx is pressed
void _EXA1(byte); //SKNP Vx - Skip next instruction if key with value of Vx is NOT pressed
void _FX07(byte); //LD Vx, DT - Set Vx = delay timer value
void _FX0A(byte); //LD Vx, K - Wait for keypress, store value in Vx
void _FX15(byte); //LD DT, Vx - Set delay timer = Vx
void _FX18(byte); //LD ST, Vx - Set sound timer = Vx
void _FX1E(byte); //ADD I, Vx - Set I += Vx
void _FX29(byte); //LD F, Vx - Set I = location of sprite for digit in Vx
void _FX33(byte); //LD B, Vx - Store BCD representation of value in Vx in memory locations I, I+1 and I+2
void _FX55(byte); //LD [I], Vx - Store registers V0 to Vx in memory starting at address in I 
void _FX65(byte); //LD Vx, [I] - Read registers V0 to Vx from memory starting at address in I

#endif
