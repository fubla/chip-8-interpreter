#include <stdlib.h>
#include <stdio.h>
#include "ops.h"

void _00EE(void)
{
	PC = stack[SP--];
}

void _1NNN(address addr)
{
	PC = addr;
}

void _2NNN(address addr)
{
	stack[++SP] = PC;
	PC = addr;
}

void _3XNN(byte reg, byte value)
{
	if(V[reg] == value)
		PC += 2;
}

void _4XNN(byte reg, byte value)
{
	if(V[reg] != value)
		PC += 2;
}

void _5XY0(byte reg1, byte reg2)
{
	if(V[reg1] == V[reg2])
		PC += 2;
}

void _6XNN(byte reg, byte value)
{
	V[reg] = value;
}

void _7XNN(byte reg, byte value)
{
	V[reg] += value;
}

void _8XY0(byte reg1, byte reg2)
{
	V[reg1] = V[reg2];
}

void _8XY1(byte reg1, byte reg2)
{
	V[reg1] |= V[reg2];
}

void _8XY2(byte reg1, byte reg2)
{
	V[reg1] &= V[reg2];
}

void _8XY3(byte reg1, byte reg2)
{
	V[reg1] ^= V[reg2];
}

void _8XY4(byte reg1, byte reg2)
{
	V[reg1] += V[reg2];
	if ( (int)V[reg1] + (int)V[reg2] > 255)
		V[15] = 1;
	else
		V[15] = 0;
}

void _8XY5(byte reg1, byte reg2)
{
	V[reg1] -= V[reg2];
	if(V[reg1] > V[reg2])
		V[15] = 1;
	else
		V[15] = 0;
}

void _8XY6(byte reg)
{
	if(V[reg] & 1)
		V[15] = 1;
	else
		V[15] = 0;
	V[reg] >>= 1; 
}

void _8XY7(byte reg1, byte reg2)
{
	V[reg1] = V[reg2] - V[reg1];
	if(V[reg2] > V[reg1])
		V[15] = 1;
	else
		V[15] = 0;
}

void _8XYE(byte reg)
{
	if(V[reg] & 0x80)
		V[15] = 1;
	else
		V[15] = 0;
	V[reg] <<= 1;
}

void _9XY0(byte reg1, byte reg2)
{
	if(V[reg1] != V[reg2])
		PC += 2;
}

void _ANNN(address addr)
{
	I = addr;
}

void _BNNN(address addr)
{
	PC = addr + V[0];
}

void _CXNN(byte reg, byte value)
{
	time_t t;
	srand((unsigned) time(&t));
	V[reg] = value + (byte)(rand()%256);
}

void _DXYN(byte reg1, byte reg2, byte n)
{
	int collision = 0;
	for(int i = 0; i < n; i++)
    {
		for(int j = 0; j < 8; j++)
        {
			byte before = display[ ((V[reg1] + j) % WIDTH) + WIDTH * ((V[reg2] + i) % HEIGHT) ];
			display[ ((V[reg1] + j) % WIDTH) + WIDTH * ((V[reg2] + i) % HEIGHT) ] ^=
			(memory[ I + i ] & (0x80 >> j)) >> (8-j);
			if(before > display[ ((V[reg1] + j) % WIDTH) + WIDTH * ((V[reg2] + i) % HEIGHT) ])
				collision = 1;
		}
	}
}

void _EX9E(byte reg)
{
	if(keypress == V[reg])
		PC += 2;
}

void _EXA1(byte reg)
{
	if(keypress != V[reg])
		PC += 2;
}

void _FX07(byte reg)
{
	V[reg] = delaytimer;
}

void _FX0A(byte reg)
{
	V[reg] = getchar();
}

void _FX15(byte reg)
{
    delaytimer = V[reg];
}

void _FX18(byte reg)
{
    soundtimer = V[reg];
}

void _FX1E(byte reg)
{
    I += V[reg];
}

void _FX29(byte reg)
{
    I = fontdata[V[reg] * 5];   
}

void _FX33(byte reg)
{
    unsigned int tmp = (unsigned int)reg;
    for(int i = 0; i < 8; i++)
    {
        tmp <<= 1;
        if(tmp & 0xf00 >= 0x500)
        {
            tmp += 0x300;
        }
        else if(tmp & 0xf000 >= 0x5000)
        {
            tmp += 0x3000;
        }
        else if(tmp & 0xf0000 >= 0x50000)
        {
            tmp += 0x30000;
        }
    }
}

