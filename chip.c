#include <time.h>
#include <stdio.h>
#include "chip.h"
#include "ops.h"



void init(void)
{
	reset(memory, MEMORYSIZE);
	reset(display, WIDTH*HEIGHT);
	reset(V, REGS);
	resetstack(stack, STACKSIZE*2);
	I = 0;
	SP = 0;
	PC = 512;
	delaytimer = 0;
	soundtimer = 0;
	keypress = 0;
}

int load(char *filename)
{
	int i = 512;
	FILE *fp = fopen(filename, "r");
	if(!fp)
    {
		perror("There was an error opening the file: ");
		return 0;
	}
	char b = getc(fp);
	while(b != EOF)
    {
		memory[i++] = (byte)b;
		b = getc(fp);
	}
	return 1;
}

void reset(byte *mem, size_t size)
{
	for(int i=0; i<size; i++)
    {
		mem[i] = 0;
	}
}

void resetstack(address *mem, size_t size)
{
	for(int i=0; i<size; i++)
    {
		mem[i] = 0;
	}	
}

byte dectimer(byte timer)
{
	return --timer;
}

